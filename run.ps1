docker stop xyresto-frontend-customer
docker rm xyresto-frontend-customer
docker run --name xyresto-frontend-customer `
           -p 80:80 `
		   --mount type=bind,source=$(pwd)/nginx.conf,target=/etc/nginx/nginx.conf `
		   --mount type=bind,source=$(pwd)/public,target=/var/www/html `
		   -d `
		   nginx:latest 