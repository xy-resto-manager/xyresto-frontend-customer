$.ajax({
    type: 'GET',
    url: '/backend/api/user/current-user',
    error: (e) => {
        window.location.href = '/login';
    }
});