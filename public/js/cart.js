function onContentLoad() {
    if (window.location.search.includes('o=1')) {
        $('#alert-added').slideDown().delay(2000).slideUp();
        window.history.replaceState({}, document.title, "/cart");
    }

    getReservationData().done((resp) => {
        let dining = resp.data.filter(reservation => reservation.status === 'DINING').length > 0;
        if (dining) {
            setModeAddOrder();
        } else {
            setModeCreateReservation();
        }
    });

    getCurrentCart().done(processCart);
}

function getCurrentCart() {
    return $.ajax({
        type: 'GET',
        url: '/backend/api/cart/current-cart',
        error: (e) => {
            console.log(e);
        }
    });
}

function getReservationData() {
    return $.ajax({
        method: 'GET',
        url: '/backend/api/reservation/reservations',
        dataType: 'json',
        error: (e) => {
            console.log(e);
        }
    });
}

function getProductImage(orderedProduct) {
    return $.ajax({
        type: 'GET',
        url: '/backend' + orderedProduct.product.imageUrl,
        xhr: () => {
            let xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';
            return xhr;
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function setModeAddOrder() {
    $('.content header p:eq(0)').html('Add Orders');
    $('#reserve-btn').html('Order').attr('href', '#').click(() => {
        $.ajax({
            method: 'GET',
            url: '/backend/api/cart/current-cart',
            dataType: 'json',
            success: (resp) => {
                let payload = resp.data.products.map(orderedproduct => ({
                    productId: orderedproduct.product.id,
                    quantity: orderedproduct.quantity
                }));

                $.ajax({
                    method: 'PUT',
                    url: `/backend/api/reservation/reservations/${dining[0].id}/orders`,
                    contentType: 'application/json',
                    dataType: 'json',
                    data: JSON.stringify(payload),
                    success: (resp) => {
                        window.location.href = '/cart?o=1';
                    },
                    error: (e) => {
                        console.log(e);
                    }
                });
            },
            error: (e) => {
                console.log(e);
            }
        });
    });
}

function setModeCreateReservation() {
    $('.content header p:eq(0)').html('Preorder Items');
    $('#reserve-btn').html('Reserve');
}

let changeloading = false;
let totalquantity = 0;
let totalprice = 0;

function processCart(response) {
    let template = $('#card-template');
    $('.spinner').remove()
    response.data.products.forEach(orderedProduct => createOrderedProduct(template, orderedProduct));
    updateSummary();
}

function createOrderedProduct(template, orderedProduct) {
    totalquantity += orderedProduct.quantity;
    totalprice += orderedProduct.quantity * orderedProduct.product.price;
    let card = template.clone();
    card.removeAttr('id');

    if (orderedProduct.product.imageUrl != null) {
        getProductImage(orderedProduct).done(response => {
            card.find('.product-image').attr('src', URL.createObjectURL(response));
        });
    }

    card.find('.text p').html(orderedProduct.product.name);
    card.find('.text small').html('Rp. ' + orderedProduct.product.price);
    card.find('.edit-orders p').html(orderedProduct.quantity);

    let amount = orderedProduct.quantity;
    card.find('.product-subtract').click(() => {
        if (changeloading) return;
        changeloading = 1;
        amount--;
        editProduct(card, orderedProduct.product.id, orderedProduct.product.price, amount, -1);
    });
    card.find('.product-add').click(() => {
        if (changeloading) return;
        changeloading = 1;
        amount++;
        editProduct(card, orderedProduct.product.id, orderedProduct.product.price, amount, 1);
    });
    card.find('.delete-item').click(() => {
        editProduct(card, orderedProduct.product.id, orderedProduct.product.price, 0, -amount);
    });

    $('.card-placeholder').append(card);

    updateSummary();
}

function editProduct(card, id, price, amount, delta) {
    let postData = {
        'productId': id,
        'quantity': amount
    };
    $.ajax({
        type: 'PUT',
        url: '/backend/api/cart/current-cart',
        data: JSON.stringify(postData),
        contentType: 'application/json',
        dataType: 'json',
        success: (response) => {
            totalquantity += delta;
            totalprice += delta * price;
            updateSummary();
            if (amount === 0) {
                card.fadeOut(400, () => card.remove());

            } else {
                card.find('.edit-orders p').html(amount);
            }
            changeloading = 0;
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function updateSummary() {
    $('#product-count span').html(totalquantity);
    $('#total-order').html('Rp. ' + totalprice + ',-');
}
