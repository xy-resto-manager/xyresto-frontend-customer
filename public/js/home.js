function main(){
    getBanners().done(loadBanners);
    getTopMenus();
    getRecommendedMenus();

    $(window).resize(() => {
        let width = $('.banners').width()
        $('.banner').width(width);
        $('.banner-carousel').css('transform', 'translateX(-' + bannerpos * width + 'px)');
    });
}

function getBanners(){
    return $.ajax({
        type: 'GET',
        url: '/backend/api/banner/banners',
        dataType : 'json',
        error: function(e){
            console.log(e)
        }
    })
}

function loadBanners(response){
    $('.placeholder-banner').remove();
    let len = response.data.length;
    let banners = $('.banners');
    let bannerCarousel = $('.banner-carousel');
    bannerCarousel.css('width',  len * 100 + '%');

    let template = $('#banner-template').clone();
    response.data.forEach(banner => displayBanner(template, bannerCarousel, banner));

    if (len > 0) {
        let dots = $('.dots');
        dots.append(
            $('<span></span>').addClass('dot-active').click(() => {
                setBanner(0);
            })
        );
        for (let i = 0; i < len-1; i++) {
            dots.append(
                $('<span></span>').addClass('dot').click(() => {
                    setBanner(i+1);
                })
            );
        }
    }

    setInterval(() => {
        loopBanners(len);
    }, 5000);
}

function displayBanner(oldtemplate, bannerCarousel, banner) {
    let template = oldtemplate.clone();
    template.removeAttr('id');
    bannerCarousel.append(template);

    $.ajax({
        url : '/backend' + banner.imageUrl,
        xhr: () => {
            let xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';
            return xhr;
        },
        success: (response) => {
            template.find('img').attr('src', URL.createObjectURL(response));
            template.outerWidth($('.banners').width());
        },
        error: (e) => {
            console.log(e)
        }
    })
}

let bannerpos = 0;
function loopBanners(len) {
    let newpos = bannerpos + 1;
    if (newpos === len) newpos = 0;
    setBanner(newpos);
}

function setBanner(index) {
    bannerpos = index;
    $('.dot-active').removeClass('dot-active').addClass('dot');
    $('.dots').children().eq(index).removeClass('dot').addClass('dot-active');
    $('.banner-carousel').css('transform', 'translateX(-' + index * $('.banners')[0].offsetWidth + 'px)');
}

function getTopMenus() {
    $.ajax({
        type: 'GET',
        url: '/backend/api/product/products',
        data: null,
        dataType: 'json',
        success: (response) => {
            $('.placeholder-card-top').remove();
            loadMenus(response, $('.top-dishes'));
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function getRecommendedMenus() {
    $.ajax({
        type: 'GET',
        url: '/backend/api/product/products?sort=recommended',
        dataType: 'json',
        success: (response) => {
            $('.placeholder-card-recommended').remove();
            loadMenus(response, $('.recommended-dishes'));
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function loadMenus(response, parent) {
    response.data.forEach(product => {
        let template = $('#card-template').clone();
        template.removeAttr('id');
        template.find(".detail").find(".product-name").html(product.name);
        template.find(".detail").find(".product-price").html("Rp. " + product.price);

        template.find(".add-to-cart").click(() => {
            getCart().done((resp) => {
                let filtered = resp.data.products.filter(orderedproduct => orderedproduct.product.id === product.id);
                let quantity = 1;
                if (filtered.length > 0) {
                    quantity = filtered[0].quantity + 1;
                }
                modifyCart(product.id, quantity).done(() => {
                    template.find('.cart-added').fadeIn(300).delay(200).fadeOut(300);
                });
            });
        });

        parent.append(template)

        if (product.imageUrl == null) return;

        $.ajax({
            type: 'GET',
            url: '/backend' + product.imageUrl,
            xhr: () => {
                let xhr = new XMLHttpRequest();
                xhr.responseType = 'blob';
                return xhr;
            },
            success: (response) => {
                template.find('.product-image').attr('src', URL.createObjectURL(response));
            },
            error: function(e) {
                console.log(e);
            }
        });
    });
}

function getCart() {
    return $.ajax({
        method: 'GET',
        url: '/backend/api/cart/current-cart',
        contentType: 'application/json',
        dataType: 'json',
        error: (e) => {
            console.log(e);
        }
    });
}

function modifyCart(productId, quantity) {
    let payload = {
        productId: productId,
        quantity: quantity
    };
    return $.ajax({
        method: 'PUT',
        url: '/backend/api/cart/current-cart',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(payload),
        error: (e) => {
            console.log(e);
        }
    });
}
