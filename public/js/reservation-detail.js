let url = window.location.href;
let reservationId = url.slice(url.lastIndexOf('/') + 1)

function onContentLoad() {
    loadReservationData().done(response => {
        $('.spinner').remove();
        showReservationData(response.data);
    })

    $('.cancel-reservation').click(() => {
        $('.overlay').css({ opacity: 0, display: 'flex' }).animate({
            opacity: 1
        }, 150);
    });

    $('#button-exit').click(() => {
        $('.overlay').animate({ opacity: 0 }, 150)
    })
    $('#button-no').click(() => {
        $('.overlay').animate({ opacity: 0 }, 150)
    })
    $('#button-yes').click(cancelReservation);
}

function cancelReservation() {
    let postData = {
        canceled: true
    }
    $.ajax({
        type: 'PUT',
        url: '/backend/api/reservation/reservations/' + reservationId,
        data: JSON.stringify(postData),
        contentType: 'application/json',
        dataType: 'json',
        success: () => {
            window.location.href = '/reservation';
        },
        error: (e) => {
            console.log(e);
        }
    })
}

function loadReservationData() {
    return $.ajax({
        type: 'GET',
        url: '/backend/api/reservation/reservations/' + reservationId,
        dataType: 'json',
        error: function (e) {
            console.log(e)
        }
    });
}

function showReservationData(data) {
    let time = new Date(data.timestamp)
    let year = time.getFullYear().toString();
    let month = time.getMonth();
    let date = time.getDate().toString();
    let day = time.getDay();
    let hour = time.getHours().toString();
    let nextHour = (time.getHours() + 1).toString();


    //Text Data
    $('#template-detail-text').hide();
    $('#menu-card-template').hide();
    let textTemplate = $('#template-detail-text').clone().show();
    textTemplate.find('#booking-id').find('span').text(data.id)
    textTemplate.find('#date').find('span').text(getDayName(day) + date + getMonthName(month) + year)
    textTemplate.find('#time').find('span').text(hour + ":00 - " + nextHour + ":00")
    textTemplate.find('#table-no').find('span').text(data.tableId)
    textTemplate.find('#img-status').attr('src', getStatusImage(data.status))
    $('#data').show().prepend(textTemplate)

    //Orders data
    data.orders.forEach(product => {
        let menuCardTemplate = $('#menu-card-template').clone().show();
        menuCardTemplate.find('#product-details').children().find("#product-name").text(product.product.name)
        menuCardTemplate.find('#product-details').children().find("#product-quantity > span").text(product.quantity)
        menuCardTemplate.find('#product-details').children().find("#product-price > span").text(product.product.price + ",00")

        if (product.product.imageUrl != null) {
            getProductImage(product.product).done(response => {
                menuCardTemplate.find('#product-image').attr('src', URL.createObjectURL(response));
            });
        }
        $('#menu-list').append(menuCardTemplate)
    });

    if (data.status === "PENDING") {
        $('.cancel-reservation').show()
    } else {
        $('.cancel-reservation').remove()
    }
}

function getProductImage(product) {
    return $.ajax({
        type: 'GET',
        url: '/backend' + product.imageUrl,
        xhr: () => {
            let xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';
            return xhr;
        },
        error: function (e) {
            console.log(e);
        }
    });
}

const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
function getDayName(day) {
    return `${DAYS[day]}, `;
}

const MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
function getMonthName(month) {
    return ` ${MONTHS[month]} `;
}

function getStatusImage(status) {
    return `/img/status/${status.toLowerCase()}.svg`;
}
