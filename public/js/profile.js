var changepass_shown = 0;

function onContentLoad() {
    $.ajax({
        type: 'GET',
        url: '/backend/api/user/current-user',
        dataType: 'json',
        success: (resp) => {
            $('#profile-name').html(resp.data.fullName);
        },
        error: (e) => {
            console.log(e);
        }
    });

    $('.logout-modal').click((e) => {
        e.stopPropagation();
    })
    $('.overlay').click(function() {
        $(this).fadeOut(150);
    });
    $('#logout-button-no').click(() => {
        $('.overlay').fadeOut(150);
    })
    $('#logout-button-exit').click(() => {
        $('.overlay').fadeOut(150);
    })

    $('#option-logout').click(() => {
        $('.overlay').css({opacity: 0, display: 'flex'}).animate({
            opacity: 1
        }, 150);
    });

    $('#logout-button-yes').click(() => {
        $.ajax({
            type: 'POST',
            url: '/backend/api/user/_logout',
            success: (resp) => {
                window.location.href = '/';
            },
            error: (e) => {
                console.log(e);
            }
        });
    });

    $('#option-password').click(() => {
        changepass_shown = 1;
        $('.change-password-panel').css('transform', 'translateX(0)')
    });

    $('#changepass-back').click(() => {
        changepass_shown = 0;
        $('.change-password-panel').css('transform', 'translateX(101%)')
    });

    $('#changepass-submit').click(tryChangepass);

    $(document).keypress((e) => {
        if (e.which == 13) tryChangepass();
    })
}

function tryChangepass() {
    if (changepass_shown !== 1) return;
    $('#changepass-wrongconfirm').hide();
    $('#changepass-wrongpassword').hide();
    $('#changepass-success').hide();
    $('#changepass-unknownerror').hide();
    $('#changepass-samepassword').hide();

    if (!$('.change-password-content form')[0].checkValidity()) {
        $('<input type="submit">').hide().appendTo($('.change-password-content form')).click().remove();
        return;
    }

    let old = $('#changepass-old').val();
    let new1 = $('#changepass-new').val();
    let new2 = $('#changepass-confirm').val();

    if (new1 !== new2) {
        $('#changepass-wrongconfirm').show();
        return;
    }

    if (old === new1) {
        $('#changepass-samepassword').show();
        return;
    }

    let postData = {
        'oldPassword': old,
        'newPassword': new1
    }

    $.ajax({
        type: 'POST',
        url: '/backend/api/user/_change-password',
        data: JSON.stringify(postData),
        contentType: 'application/json',
        dataType: 'json',
        success: (resp) => {
            $('#changepass-success').show();
            $('.change-password-content input:not([type=button])').val('');
        },
        error: (e) => {
            if (e.responseJSON.errors.find(error => error.error === 'FailedAuthenticationException') !== undefined) {
                $('#changepass-wrongpassword').show();
                $('.change-password-content input:not([type=button])').val('');
            } else {
                $('#changepass-unknownerror').show();
            }
        }
    });
}