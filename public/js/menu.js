var page = 1;
var itemsPerPage;
var totalItems;
var curCategory = -1;

function onContentLoad() {
    $('.card-list').scroll(function() {
        if ($(this).scrollTop() + $(this).height() + 10 >= $(this).prop('scrollHeight')) {
            if (page * itemsPerPage < totalItems)
                getMoreMenus().done(loadMoreMenus);
        }
    });

    $('.search input').on('input', function() {
        search($(this).val());
    });

    getCategories();
    getMenus();
}

function search(query) {
    page = 1;
    if (query.length === 0) {
        $('.categories').show();
        getCategories();
        getMenus();
        return;
    }

    $('.search input').off('input');
    $('.spinner').show();
    getSearchedMenu(query).done((response) => {
        $('.spinner').hide();
        $('.categories').hide();
        $('.card:not(#card-template)').remove();

        $('.search input').on('input', function() {
            search($(this).val());
        });

        totalItems = response.pagination.totalItems;
        loadMenus(response);
    });
}

function getSearchedMenu(query) {
    return $.ajax({
        type: 'GET',
        url: '/backend/api/product/products?page=' + page + '&query=' + query,
        dataType: 'json',
        error: function (e) {
            console.log(e)
        }
    });
}

function getMoreMenus() {
    page++;
    $('.menu').append($('<div></div>').addClass('spinner').addClass('extra-spinner'));
    return $.ajax({
        type: 'GET',
        url: '/backend/api/product/products?page=' + page + (curCategory !== -1 ? '&sort=' + curCategory : ''),
        dataType: 'json',
        error: function (e) {
            console.log(e)
        }
    })
}

function loadMoreMenus(response) {
    $('.extra-spinner').remove();

    let template = $('#card-template');
    let list = $('.card-list');
    response.data.forEach(product => {
        displayMenu(template, list, product)
    });
}

function displayMenu(oldtemplate, list, product) {
    let template = oldtemplate.clone();
    template.removeAttr('id');
    list.append(template);

    if (product.imageUrl != null) {
        getProductImage(product).done((response) => {
            template.find('.product-image').attr('src', URL.createObjectURL(response));
        });
    }

    let productname = template.find('.product-name');
    productname.find('p').html(product.name);

    product.traits.forEach(trait => {
        productname.append($('<img>').addClass('traits-icon').attr('src', '/img/' + trait.toLowerCase() + '.svg'));
    });

    template.find('small').html(product.description);
    template.find('.price-tag').html('Rp ' + product.price);
    template.find('.price-and-btn a').click(() => {
        getCart().done(response => {
            addToCart(response, template.find('.price-and-btn a'), product.id);
        });
    });
}

function getProductImage(product) {
    return $.ajax({
        type: 'GET',
        url: '/backend' + product.imageUrl,
        xhr: () => {
            let xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';
            return xhr;
        },
        error: function (e) {
            console.log(e);
        }
    });
}

function changeCategory(category, name) {
    if (curCategory === category) return;
    page = 1;
    curCategory = category;
    $('.card:not(#card-template)').remove();
    $('.extra-spinner').remove();
    $('.spinner').show();
    $('.carousel').removeClass('carousel-active');
    $('.carousel:contains(\'' + name + '\')').addClass('carousel-active');
    $.ajax({
        type: 'GET',
        url: '/backend/api/product/products?sort=' + category,
        dataType: 'json',
        success: loadMenus,
        error: function (e) {
            console.log(e)
        }
    })
}

function getCategories() {
    $('.carousel:not(#carousel-template)').remove();
    $.ajax({
        type: 'GET',
        url: '/backend/api/product/categories',
        dataType: 'json',
        success: loadCategories,
        error: function (e) {
            console.log(e)
        }
    })
}

function loadCategories(response) {
    response.data.forEach(category => {
        let template = $('#carousel-template').clone();
        template.removeAttr('id');
        $('.categories-carousel').append(template);

        template.click(() => {
            changeCategory(category.id, category.name)
        });
        template.html(category.name);
    })
}

function getMenus() {
    if (page === 1) {
        $('.card:not(#card-template)').remove();
    }
    $.ajax({
        type: 'GET',
        url: '/backend/api/product/products?page=' + page,
        dataType: 'json',
        success: loadMenus,
        error: function (e) {
            console.log(e)
        }
    })
}

function loadMenus(response) {
    totalItems = response.pagination.totalItems;
    itemsPerPage = response.pagination.itemsPerPage;
    $('.spinner').hide();

    let template = $('#card-template');
    let list = $('.card-list');
    response.data.forEach(product => {
        displayMenu(template, list, product);
    });
}

function getCart() {
    return $.ajax({
        type: 'GET',
        url: '/backend/api/cart/current-cart',
        dataType: 'json',
        error: (e) => {
            if (e.responseJSON.errors.find(error => error.error === 'UnauthorizedAccessException') !== undefined) {
                window.location.href = '/login';
            } else {
                console.log(e);
            }
        }
    })
}

function addToCart(response, element, id) {
    let filtered = response.data.products.filter(orderedproduct => orderedproduct.product.id === id);
    let quantity = 1;
    if (filtered.length > 0) {
        quantity = filtered[0].quantity + 1;
    }
    let payload = {
        productId: id,
        quantity: quantity
    };
    $.ajax({
        method: 'PUT',
        url: '/backend/api/cart/current-cart',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(payload),
        success: (resp) => {
            let label = $('<div></div>').addClass('cart-added').html("Added to cart!").hide();
            element.append(label);
            label.fadeIn(300).delay(200).fadeOut(300);
        },
        error: (e) => {
            if (e.responseJSON.errors.find(error => error.error === 'UnauthorizedAccessException') !== undefined) {
                window.location.href = '/login';
            } else {
                console.log(e);
            }
        }
    });
}
