let currentDate = null;
let currentTime = "09:00:00"; // Default Value
let canvas = null;
let ctx = null;
let width = null;
let height = null;
let selectedTable = null;
let selectedTableX = null;
let selectedTableY = null;
let tableData = null;
let gridWidth = null;
let gridHeight = null;
let tileWidth = null;
let tileHeight = null;
let timestamp = null;
let cartData = null;

function onContentLoad(){
    canvas = document.querySelector('#canvas');
    ctx = canvas.getContext('2d');
    width = Math.round(canvas.offsetWidth);
    height = Math.round(canvas.offsetHeight);
    ctx.canvas.width = width;
    ctx.canvas.height = height;
    gridWidth = width / 5;
    gridHeight = height / 5;
    tileWidth = gridWidth * 0.8;
    tileHeight = gridHeight * 0.8;

    $('#input-date').change(() => {
        currentDate = $('#input-date').val()
        getReservationOnTimeStamp();
    });

    $('#input-time').change(() => {
        currentTime = $('#input-time').val()
        getReservationOnTimeStamp();
    });

    $('#canvas').click((e) => {
        checkIfTableIsAvailable(e);
    });

    $('.close-modal-btn').click(() => {
        $('.overlay').hide();
    });

    $('.reserve-btn').click(() => {
        if(selectedTable != null){
            $('.confirm-reservation-overlay').css({transform:'translateX(0)','z-index':"2"}).animate({ opacity: 1 }, 150);
            updateConfirmationReservation();
        }
    })
    $('#chevron-back-overlay').click(() => {
        $('.confirm-reservation-overlay').css({transform:'translateX(101%)'}).animate({ opacity: 0 }, 150);
    })

    $('.confirm-reservation-btn').click(confirmReservation);
}

function confirmReservation() {
    let orders = []
    cartData.products.forEach(orderedProduct => {
        let order = {
            productId : orderedProduct.product.id,
            quantity : orderedProduct.quantity
        }
        orders.push(order);
    });
    let postData = {
        timestamp : timestamp,
        tableId : selectedTable,
        orders : orders
    }
    $.ajax({
        type: 'POST',
        url: '/backend/api/reservation/reservations',
        data: JSON.stringify(postData),
        contentType: 'application/json',
        dataType: 'json',
        success: reservationSuccess,
        error: (e) => {
            if (e.responseJSON.errors[0].error == "ExpiredDateException") {
                showModal('Expired')
            } else if (e.responseJSON.errors[0].error == "IsBookedException") {
                showModal("Booked")
            } else {
                console.log(e);
            }
        }
    });
}

function reservationSuccess() {
    window.location.href = '/reservation';
}

function getReservationOnTimeStamp() {
    if(currentDate != null && currentTime != null){
        ctx.clearRect(0,0,width,height);
        // Ini masih local time, mau disimpen di database UTC / GMT jadi dikarenakan timezone kita GMT+7 dikurangi 7 jam
        timestamp = new Date(currentDate + "T" + currentTime+"Z").getTime() - 7 * 3600000;
        $.ajax({
            type: 'GET',
            url: '/backend/api/layout/tables/?timestamp='+timestamp,
            dataType : 'json',
            success : initLayout,
            error: function(e) {
                console.log(e)
            }
        });
    }
}

function initLayout(response){
    tableData = response.data

    response.data.forEach(table => {
        var startXDrawPos = (table.table.x - 1) * gridWidth + 0.1 * gridWidth;
        var startYDrawPos = (table.table.y - 1) * gridHeight + 0.1 * gridHeight;
        var tableNo = table.table.tableId;
        ctx.beginPath();
        ctx.font = '20px serif';
        if (table.booked) {
            drawBooked(startXDrawPos, startYDrawPos, tableNo);
        } else {
            drawAvailable(startXDrawPos, startYDrawPos, tableNo);
        }
    });
}

function checkIfTableIsAvailable(e) {
    let xPos = Math.ceil(e.offsetX / gridWidth)
    let yPos = Math.ceil(e.offsetY / gridHeight)

    tableData.forEach(table => {
        var startXDrawPos = (table.table.x - 1) * gridWidth + 0.1 * gridWidth;
        var startYDrawPos = (table.table.y - 1) * gridHeight + 0.1 * gridHeight;
         if (table.table.x == xPos && table.table.y == yPos) {
             if (table.booked) {
                showModal('Booked')
             } else {
                 if (selectedTable == null) {
                     selectedTable = table.table.tableId;
                     selectedTableX = table.table.x;
                     selectedTableY = table.table.y;
                     drawSelected(startXDrawPos,startYDrawPos,selectedTable)

                 } else {
                     if (selectedTable == table.table.tableId) {
                        drawAvailable(startXDrawPos,startYDrawPos,selectedTable);
                        selectedTable = null;
                        selectedTableX = null;
                        selectedTableY = null;
                     } else {
                        var resetRectStartXDrawPos = (selectedTableX - 1) * gridWidth + 0.1 * gridWidth;
                        var resetRectStartYDrawPos = (selectedTableY - 1) * gridHeight + 0.1 * gridHeight;
                        drawAvailable(resetRectStartXDrawPos,resetRectStartYDrawPos,selectedTable);
                        //
                        selectedTable = table.table.tableId;
                        selectedTableX = table.table.x;
                        selectedTableY = table.table.y;
                        drawSelected(startXDrawPos,startYDrawPos,selectedTable)
                     }
                 }
             }
         }
    });
    $('.table-no > span').text(selectedTable)
    $('.capacity > span').text(4)
}

function showModal(string){
    if(string == "Booked"){
        $('.modal > p').text('Table is Booked!')
    }else if(string == "Expired"){
        $('.modal > p').text("Expired Date!")
    }
    $('.overlay').css({display: 'flex',"z-index" : 3}).animate({
        opacity: 1
    }, 150).show();
}

function drawBooked(startXDrawPos, startYDrawPos, tableNo){
    ctx.beginPath();
    ctx.fillStyle = "#F9F7F0";
    ctx.fillRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.fillStyle = "#242424";
    ctx.strokeRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.fillText(tableNo,(startXDrawPos + 0.35 * gridWidth),(startYDrawPos + 0.5 * gridHeight));
}

function drawAvailable(startXDrawPos, startYDrawPos, tableNo){
    ctx.beginPath();
    ctx.clearRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.strokeRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.fillStyle = "#242424";
    ctx.fillText(tableNo,(startXDrawPos + 0.35 * gridWidth),(startYDrawPos + 0.5 * gridHeight));
}

function drawSelected(startXDrawPos, startYDrawPos, tableNo) {
    ctx.beginPath();
    ctx.clearRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.fillStyle = "#E4C102";
    ctx.fillRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.fillStyle = "#F9F7F0";
    ctx.fillText(tableNo,(startXDrawPos + 0.35 * gridWidth),(startYDrawPos + 0.5 * gridHeight));
}

function updateConfirmationReservation() {
    let date = getDate();
    let time = getTime();
    $('#display-date > span').html(date);
    $('#display-time > span').html(time);
    $('#display-tableNo > span').html(selectedTable);

    $.ajax({
        type: 'GET',
        url: '/backend/api/cart/current-cart',
        success: processCart,
        error: (e) => {
            console.log(e);
        }
    });

}

function getDate() {
    let time = new Date(timestamp);
    let year = time.getFullYear().toString();
    let month = time.getMonth();
    let day = time.getDay();
    let date = time.getDate().toString();

    return `${getDayName(day)}${getMonthName(month)} ${date}th ${year}`;
}

function getTime(){
    let time = new Date(timestamp);

    let hour = time.getHours().toString();
    let nextHour = (time.getHours() + 1).toString();

    return `${hour}:00 - ${nextHour}:00`;
}

const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
function getDayName(day) {
    return `${DAYS[day]}, `;
}

const MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
function getMonthName(month) {
    return ` ${MONTHS[month]} `;
}

function processCart(response){
    cartData = response.data;
    let totalPrice = 0;
    response.data.products.forEach(orderedProduct => {
        let template = $('#menu-card-template').clone();
        template.removeAttr('id');
        if (orderedProduct.product.imageUrl != null) {
            $.ajax({
                type: 'GET',
                url: '/backend' + orderedProduct.product.imageUrl,
                xhr: () => {
                    let xhr = new XMLHttpRequest();
                    xhr.responseType = 'blob';
                    return xhr;
                },
                success: (response) => {
                    template.find('.product-image').attr('src', URL.createObjectURL(response));
                },
                error: function(e) {
                    console.log(e);
                }
            });
        }
        template.find('#product-name > span').text(orderedProduct.product.name)
        template.find('#product-quantity > span').text(orderedProduct.quantity)
        template.find('#product-price > span ').text(orderedProduct.product.price)
        totalPrice += orderedProduct.quantity * orderedProduct.product.price
        $('#menu-list').append(template);
    });
    $('#menu-card-template').hide();
    $('#total-price > span').text(totalPrice)
}
