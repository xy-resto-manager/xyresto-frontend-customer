$.ajax({
    type: 'GET',
    url: '/backend/api/user/current-user',
    error: (e) => {
        window.location.href = '/login';
    }
});

function onContentLoad() {
    getUserReservation().done((resp) => {
        $('.spinner').hide();
        showReservationData(resp.data)
    });
}

function getUserReservation() {
    return $.ajax({
        type: 'GET',
        url: '/backend/api/reservation/reservations',
        dataType : 'json',
        error: function(e){
            console.log(e)
        }
    })

}

function showReservationData(data){
    if (data.length === 0) {
        $('#reservation-list').show().css({"display":"flex","align-items":"center","justify-content":"center"});
        $(".no-reservation").show();
        $("#template-card").hide();
    } else {
        $('#template-card').hide();
        data.forEach(element => {
            console.log(element)
            //New Date disini otomatis jadi Local time, jadi dari database + 7 Jam
            let time = new Date(element.timestamp)
            let year = time.getFullYear().toString();
            let month = time.getMonth();
            let date = time.getDate().toString();
            let hour = time.getHours().toString();
            let nextHour = (time.getHours()+1).toString();
            let template = $('#template-card').clone().show();
            template.removeAttr('id');
            template.find('.date').find('span').text(date + getMonth(month) + year)
            template.find('.time').find('span').text(hour+":00 - "+nextHour+":00")
            template.find('.table-number').find('span').text(element.tableId)
            template.find('.align-center').find('a').attr("href","reservations/"+element.id)
            template.find('img').attr('src',getStatusImage(element.status))
            $('#reservation-list').show().append(template);
        });
    }
}

const MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
function getMonth(month) {
    return ` ${MONTHS[month]} `;
}

function getStatusImage(status) {
    return `/img/status/${status.toLowerCase()}.svg`;
}
